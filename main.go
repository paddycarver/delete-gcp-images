package main

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/hashicorp/terraform/helper/pathorcontents"
	compute "google.golang.org/api/compute/v1"
	"google.golang.org/api/option"
	yall "yall.in"
	"yall.in/colour"
)

func main() {
	log := yall.New(colour.New(os.Stdout, yall.Debug))
	project := os.Getenv("GOOGLE_PROJECT")
	if project == "" {
		log.Error("GOOGLE_PROJECT must be set to the project to delete images from")
		os.Exit(1)
	}
	credentials := os.Getenv("GOOGLE_CREDENTIALS")
	if credentials == "" {
		log.Error("GOOGLE_CREDENTIALS must be set to the JSON contents or the path to a file containing the JSON contents of a service account's credentials")
		os.Exit(1)
	}
	contents, _, err := pathorcontents.Read(credentials)
	if err != nil {
		log.WithError(err).WithField("credentials", credentials).Error("Error reading credentials")
		os.Exit(1)
	}
	comp, err := compute.NewService(context.Background(), option.WithCredentialsJSON([]byte(contents)))
	if err != nil {
		log.WithError(err).Error("Error creating cloud resource manager client")
		os.Exit(1)
	}
	var deleting, failed, pending, total int
	var images []string
	err = comp.Images.List(project).Pages(context.Background(), func(resp *compute.ImageList) error {
		for _, image := range resp.Items {
			total++
			switch image.Status {
			case "DELETING":
				deleting++
			case "FAILED":
				failed++
			case "PENDING":
				pending++
			}

			// skip any images you don't want deleted. https://google.golang.org/api/compute/v1#Image
			// has details on what info you can read from each image to decide whether you want to
			// delete it or not. Any images that get added to the `images` slice will be deleted.
			if image.Status != "READY" && image.Status != "FAILED" {
				continue
			}
			if !strings.HasPrefix(image.Name, "terraform-") {
				continue
			}
			// dnoe modifying

			images = append(images, image.Name)
		}
		return nil
	})
	if err != nil {
		log.WithError(err).Error("Error listing images")
		os.Exit(1)
	}
	log.WithField("total_images", total).WithField("images_pending_creation", pending).WithField("images_pending_deletion", deleting).WithField("failed_images", failed).Info("found images")
	log.Info("Going to delete " + strconv.Itoa(len(images)) + " images:")
	for _, image := range images {
		log.Info("\t" + image)
	}
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("Enter 'yes' to delete: ")
	resp, err := reader.ReadString('\n')
	if err != nil {
		log.WithError(err).Error("error reading input")
		os.Exit(1)
	}
	resp = strings.Replace(resp, "\n", "", -1)
	if resp != "yes" {
		log.WithField("response", resp).Error("'yes' not entered, aborting")
		os.Exit(1)
	}
	for _, image := range images {
		_, err = comp.Images.Delete(project, image).Do()
		l := log.WithField("image", image)
		if err != nil {
			l.WithError(err).Error("Error deleting image")
			os.Exit(1)
		}
		l.Info("deleted image")
	}
}
