module gitlab.com/paddycarver/delete-gcp-images

require (
	cloud.google.com/go v0.37.4 // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/hashicorp/golang-lru v0.5.1 // indirect
	github.com/hashicorp/terraform v0.11.10
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mitchellh/go-homedir v1.0.0 // indirect
	go.opencensus.io v0.20.2 // indirect
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3 // indirect
	golang.org/x/oauth2 v0.0.0-20190402181905-9f3314589c9a // indirect
	golang.org/x/sys v0.0.0-20190411185658-b44545bcd369 // indirect
	google.golang.org/api v0.3.2
	google.golang.org/appengine v1.5.0 // indirect
	google.golang.org/grpc v1.20.0 // indirect
	yall.in v0.0.1
)
